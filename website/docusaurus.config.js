/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Meine Doku Webseite',
  tagline: 'Reminder für Vergessliche',
  url: 'https://pages.gitlab.io',
  baseUrl: '/meine-doku-webseite/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'horn.sebastian', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.
  themeConfig: {
    navbar: {
      title: 'Docusaurus Medien',
      logo: {
        alt: 'Meine Doku Webseite',
        src: 'img/logo.svg',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Dokus',
          position: 'left',
        },
        //{to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://horn.sebastian.gitlab.io/meine-doku-webseite/',
          label: 'GitLab Doku Webseite',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Getting Started',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'GitLab',
              href: 'https://gitlab.com/horn.sebastian',
            },
            {
              label: 'youtube',
              href: 'https://www.youtube.com/user/hornse77',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/hornse2',
            },
          ],
        },
        {
          title: 'More',
          items: [
           // {
             // label: 'Blog',
              //to: 'blog',
            //},
            {
              label: 'GitHub Docusaurus',
              href: 'https://github.com/facebook/docusaurus',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          routeBasePath: '/',
          editUrl:
            'https://gitlab.com/horn.sebastian/meine-doku-webseite/tree/master/',//https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: false,
        //blog: {
          //showReadingTime: true,
          // Please change this to your repo.
          //editUrl:
            //'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        //},
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
